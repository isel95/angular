// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {apiKey: "AIzaSyBOr_vGvY7zY-IAh_Le8rsS4CBnEFvFXNc",
  authDomain: "angular-tarea.firebaseapp.com",
  databaseURL: "https://angular-tarea.firebaseio.com",
  projectId: "angular-tarea",
  storageBucket: "angular-tarea.appspot.com",
  messagingSenderId: "357342114104",
  appId: "1:357342114104:web:7b8c0be1ec00ef2b"

  } 
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
